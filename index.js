//cria uma referência a um elemento da página html
//estes elementos serão manipulados via programação

const imgCarro = document.getElementById('imgCarro'); // captura elemento imagem
const outPreco = document.getElementById('outPreco');

const rbFiesta = document.getElementById('rbFiesta');
const rbKa = document.getElementById('rbKa');
const rbEco = document.getElementById('rbEco');

let rbPintura = document.getElementById('rbPintura');
let rbAlarme = document.getElementById('rbAlarme');

//declara var de forma global
let preco = 49500;

function trocarFoto() {

    if (rbFiesta.checked) {
        imgCarro.src = 'fiesta.png';
        preco = 49500;

    } else if (rbKa.checked) {
        imgCarro.src = 'ka.png'
        preco = 42900
    } else {
        imgCarro.src = 'eco.png'
        preco = 58000

    }

    mostrarPreco();
    rbAlarme.checked = false;
    rbPintura.checked = false;
}

function mostrarPreco(){
    outPreco.textContent = preco.toLocaleString('pt-br', { minimumFractionDigits: 2 })
}

function somarPintura() {
    if(rbPintura.checked){
        preco = preco +1200;
    } else{
        preco = preco - 1200;
    }
    mostrarPreco()
}

function somarAlarme() {
    if(rbAlarme.checked){
        preco += 1200;
    }else{
    preco -+ 600;
    }
    mostrarPreco()
}


//adiciona 'ouvintes' de eventos para os elementos da página
//quando o evento ocorrer a função é chamada 
rbFiesta.addEventListener('change', trocarFoto);
rbKa.addEventListener('change', trocarFoto);
rbEco.addEventListener('change', trocarFoto);

rbPintura.addEventListener('change', somarPintura);
rbAlarme.addEventListener('change', somarAlarme);